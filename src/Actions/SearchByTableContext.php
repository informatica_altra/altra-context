<?php

namespace Altra\Context\Actions;

use Altra\Context\PendingTableContext;
use Altra\Context\Traits\QueryDebug;
use Altra\Context\Traits\QueryModifiers;

/**
 * This file is part of Altra,
 * Library that makes it possible to filter by context for HTTP request responses.
 *
 * @license MIT
 */
class SearchByTableContext
{
    use QueryModifiers;
    use QueryDebug;

    /** @var PendingTableContext */
    protected $context;

    /**
     * Executes the query from the table context
     *
     * @param  \Altra\Context\PendingTableContext  $context
     * @return
     */
    public function execute(PendingTableContext $context)
    {
        $this->context = $context;
        $this->prepareContext();

        return $this->queryResultWithPaginationConditional();
    }

    /**
     * Gets the raw Query that will be executed
     *
     * @param  \Altra\Context\PendingTableContext  $context
     * @return
     */
    public function getRawSql(PendingTableContext $context): string
    {
        $this->context = $context;
        $this->prepareContext();

        return $this->getContextQueryRawSqlWithoutExecuting();
    }
}
