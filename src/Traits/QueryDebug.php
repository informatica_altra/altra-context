<?php

namespace Altra\Context\Traits;

/**
 * This file is part of Altra,
 * Library that makes it possible to filter by context for HTTP request responses.
 *
 * @license MIT
 */
trait QueryDebug
{
    private function getContextQueryRawSqlWithoutExecuting(): string
    {
        $query = $this->context->query;

        return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
            return is_numeric($binding) ? $binding : "'{$binding}'";
        })->toArray());
    }
}
