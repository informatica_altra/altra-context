<?php

namespace Altra\Context\Tests\TestSupport;

use Illuminate\Database\Eloquent\Model;

class TestClassTranslation extends Model
{
    protected $guarded = false;
}
